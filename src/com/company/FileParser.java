package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class FileParser {
    private ArrayList<String> Lines = new ArrayList<>();

    FileParser(String filename) throws IOException {
        File f = new File(filename);
        if(f.exists() && !f.isDirectory()) {
            Stream<String> lines = Files.lines(Paths.get(filename), Charset.defaultCharset());
            lines.forEachOrdered(line-> {
                line = line.replaceAll("\\s+", "");
                Lines.add(line);
            });
        }else {
            throw new FileNotFoundException("Map File not found");
        }

    }
    public ArrayList<String> GetLines(){
        return Lines;
    }

}
