package com.company;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import com.company.CTree;

public class Main {
    private final static String FileDefaultPath = FileSystems.getDefault().getPath("Map.txt").toAbsolutePath().toString();
    private final static String OuputFileDefaultPath = FileSystems.getDefault().getPath("MapOutput.txt").toAbsolutePath().toString();

    public static void main(String[] args) {


        try {
            FileParser fp = new FileParser(FileDefaultPath);
             CTree tree = null;
            ArrayList<String> Lines = fp.GetLines();
            for (String line: Lines ) {
                String[] Entry = line.split("->");
                if(Entry.length==2){
                    if(tree == null){
                        tree = new CTree<String>(Entry[0]);
                        tree.addLeaf(Entry[0],Entry[1]);
                    }else{
                        tree.addLeaf(Entry[0],Entry[1]);
                    }
                }else{
                    System.out.println("Inavlid Data, unable to produce tree");
                }
            }
            Print(tree);
            boolean input = true;

            while(input){
                String q;
                Scanner sc = new Scanner(System.in);
                System.out.println("Would you like to add more nodes to the tree? Y/N: ");
                q = sc.nextLine();
                if(q.compareTo("Y")==0 || q.compareTo("y")==0){
                    String Node;
                    System.out.println("Please enter the node value like E->P");
                    Node = sc.nextLine();
                    String[] Entry = Node.split("->");
                    if(Entry.length==2){
                            tree.addLeaf(Entry[0],Entry[1]);
                    }else{
                        System.out.println("Inavlid Data, unable to produce tree");
                    }
                }else{
                    input = false;
                    Print(tree);
                }
            }


        }catch(Exception e){
            System.out.println("Unable to read file, error: "+e.getMessage());
        }
    }

    public static void Print(CTree tree) throws FileNotFoundException, UnsupportedEncodingException {
        tree.Depth = 1;
        String TreePrint = tree.toString();
        System.out.println(TreePrint);
        PrintWriter writer = new PrintWriter(OuputFileDefaultPath, "UTF-8");
        writer.print(TreePrint);
        writer.close();
        System.out.println("Identify which node is the root of the tree: Node '"+tree.getHead()+"' is the Head of the tree");
        System.out.println("Max Depth of the tree is: "+tree.depth(tree));
        System.out.println("Tree representation saved to file: "+OuputFileDefaultPath);
    }

}
