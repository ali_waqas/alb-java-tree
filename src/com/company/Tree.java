package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


class CTree<T> {
    public int Depth = 1;
    private T head;
    private ArrayList<CTree<T>> leafs = new ArrayList<CTree<T>>();
    private ArrayList<CTree<T>> deepestLeafs = new ArrayList<CTree<T>>();

    private CTree<T> parent = null;
    private HashMap<T, CTree<T>> locate = new HashMap<T, CTree<T>>();
    public CTree(T head) {
        this.head = head;
        locate.put(head, this);
    }

    public void addLeaf(T root, T leaf) {
        if (locate.containsKey(root)) {
            locate.get(root).addLeaf(leaf);
        } else {
            addLeaf(root).addLeaf(leaf);
        }
    }

    public CTree<T> addLeaf(T leaf) {
        CTree<T> t = new CTree<T>(leaf);
        leafs.add(t);
        t.parent = this;
        t.locate = this.locate;
        locate.put(leaf, t);
        return t;
    }

    public CTree<T> setAsParent(T parentRoot) {
        CTree<T> t = new CTree<T>(parentRoot);
        t.leafs.add(this);
        this.parent = t;
        t.locate = this.locate;
        t.locate.put(head, this);
        t.locate.put(parentRoot, t);
        return t;
    }

    public T getHead() {
        return head;
    }

    public CTree<T> getTree(T element) {
        return locate.get(element);
    }

    public CTree<T> getParent() {
        return parent;
    }

    public Collection<T> getSuccessors(T root) {
        Collection<T> successors = new ArrayList<T>();
        CTree<T> tree = getTree(root);
        if (null != tree) {
            for (CTree<T> leaf : tree.leafs) {
                successors.add(leaf.head);
            }
        }
        return successors;
    }

    public Collection<CTree<T>> getSubTrees() {
        return leafs;
    }

    public static <T> Collection<T> getSuccessors(T of, Collection<CTree<T>> in) {
        for (CTree<T> tree : in) {
            if (tree.locate.containsKey(of)) {
                return tree.getSuccessors(of);
            }
        }
        return new ArrayList<T>();
    }

    public int depth(CTree<String> tree){
        if(tree.getSubTrees().size()>0){
            tree.getSubTrees().forEach(st->{
                depth(st);
            });
            Depth++;
        }
        return Depth;
    }

    @Override
    public String toString() {
        return printTree(0);
    }

    private static final int indent = 2;

    private String printTree(int increment) {
        String s = "";
        String inc = "";
        for (int i = 0; i < increment; ++i) {
            inc = inc + " ";
        }
        s = inc + head;
        for (CTree<T> child : leafs) {
            s += "\n" + child.printTree(increment + indent);
        }
        return s;
    }
}
